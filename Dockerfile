FROM golang:1.14

COPY . /

WORKDIR /

RUN go build src/main.go

CMD [ "./main" ]
