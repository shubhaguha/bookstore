package config

import (
	"context"
	"fmt"
	"log"
	"os"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

// database
var DB *mongo.Database

// collections
var Books *mongo.Collection

// client
var Client *mongo.Client

func init() {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	Client, err := mongo.Connect(ctx, options.Client().ApplyURI(os.Getenv("MONGO_URI")))
	if err != nil {
		cancel()
		panic(err)
	}
	// defer Client.Disconnect(ctx)  // moved to main.go func main

	err = Client.Ping(ctx, readpref.Primary())
	if err != nil {
		cancel()
		log.Fatal(err)
	}

	DB = Client.Database("bookstore")
	Books = DB.Collection("books")

	fmt.Println("You connected to your mongo database.")
}
