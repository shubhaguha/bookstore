# bookstore

This code is from Todd McLeod's course on Udemy: [Web Development with Golang](https://github.com/GoesToEleven/golang-web-dev), section 46 on Mongo DB, subsection 16 with [full bookstore app example code](https://github.com/GoesToEleven/golang-web-dev/tree/master/046_mongodb/16_go-mongo).

The MongoDB driver was updated following the [quickstart tutorials on the MongoDB blog](https://www.mongodb.com/blog/search/golang%20quickstart). My code from these tutorials is [here](https://gitlab.com/shubhaguha/golang/-/tree/master/mongodb-blog-quickstart).

## Requirements

This project was developed using the following versions:

- Go 1.14.2
- MongoDB 4.4.0
- Docker 19.03.12
- Docker Compose 1.26.2

## Development

```bash
go mod init gitlab.com/shubhaguha/bookstore
go get go.mongodb.org/mongo-driver
go build ./...
```

## Usage

```bash
docker-compose up -d --build
```

Or:

```bash
docker run -ti -p 27017:27017 --rm mongo
MONGO_URI="mongodb://localhost:27017" go run ./src/main.go
```
